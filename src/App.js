import logo from './logo.svg';
import './App.css';
import LoginForm from "./components/LoginForm"
import Header from "./components/header";
import NavButton from './components/NavButton';
import Cooks from './components/Tables/Cooks';
import NewCook from './components/Tables/NewCook';
import CookiesManager from 'js-cookie';
import Foods from './components/Tables/Foods';
import Ingridients from './components/Tables/Ingridients';
import React from 'react';

class App extends React.Component{
  state = {
    isLoginned: false,
    token: undefined,
    name: undefined,
    activeWindow: 'car',
  }

  handleLoginClick = () => {
    if (this.state.isLoginned === false){
      this.setState({isLoginned: true})
      CookiesManager.set('isLoginned', true)
    }
    else{
      this.setState({isLoginned: false})
      CookiesManager.set('isLoginned', false);
      CookiesManager.set('token', undefined);
    }
  }
  setTokenandName = (s_token, t_name) => {
    this.setState({token: s_token, name: t_name})
    CookiesManager.set('token', s_token);
  }
  
  changeWindow = (new_window) => {
    this.setState({activeWindow: new_window})
  }

  render() {
    return (
      <div>
        {this.state.isLoginned? <div >
          <div><Header onButtonClick={this.handleLoginClick} name={this.state.name} /></div>
          <div className='MainScreen'>
            <NavButton changeWindow={this.changeWindow}/>
            {(() => {
              switch (this.state.activeWindow) {
                case 'cooks':
                  return (
                    <Cooks token = {this.state.token}/>
                  )
                case 'newcook':
                  return (
                    <NewCook token = {this.state.token}/>
                  )
                case 'foods':
                  return (
                    <Foods token = {this.state.token}/>
                  )
                case 'ingridients':
                  return (
                    <Ingridients token = {this.state.token}/>
                  )
                  
                default:
                  return (
                    <Cooks token = {this.state.token}/>
                  )
              }
              })()}
          </div>
        </div>: <LoginForm onButtonClick={this.handleLoginClick} setToken={this.setTokenandName} isLoginned={this.state.isLoginned} token={this.state.token}/>}
      </div>
    );
  }
}

export default App;