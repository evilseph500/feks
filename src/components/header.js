import React from 'react';
import '../App.css'
export default class Header extends React.Component {
    handleClick = () => {
        const {onButtonClick} = this.props;
        onButtonClick();
    }
    render(){
        return (
            <div className='App-header'>
                <div className='header'>Андреев Андрей. КТ-42-19</div>
                <div className='escapeBtn'><button onClick={this.handleClick } className='escapeBtn' >Выйти</button></div>
            </div>
        );
    }
}