import React from 'react';
const buttonArray = [
    {
      text: "Новый повар",
      icon: '',
      title: 'newcook',
    },
    {
      text: "Новая еда",
      icon: '',
      title: 'newfood',
    },
    {
      text: "Повара",
      icon: '',
      title: 'cooks',
    },
    {
      text: "Еда",
      icon: '',
      title: 'foods',
    },
    {
      text: "Ингридиенты",
      icon: '',
      title: 'ingridients',
    },
]

export default class NavButton extends React.Component{
    render(){
        const {changeWindow} = this.props;
        return(
            <div className={'navigator'}>
              {buttonArray.map((button)=>(
                <div>
                  <button className={'nav-button'} onClick={changeWindow.bind(this, button.title)}>{button.text}</button>
                </div>
              ))}
            </div>
        )
    }
}