import React from "react";
import {DataGrid, GridToolbarQuickFilter, GridActionsCellItem} from '@mui/x-data-grid'
import Box from '@mui/material/Box';
import DeleteIcon from '@mui/material/Icon';

const url = "http://darcinc.pythonanywhere.com/api/ingridient/";
function QuickSearchToolbar() {
    return(
        <Box sx={{p:0.5, pb:0}}>
            <GridToolbarQuickFilter
                quickFilterParser={(searchInput)=>searchInput.split(',').map((value)=>value.trim()).filter((value)=>value !=='')}
            />
        </Box>
    );
}
export default class Ingridients extends React.Component{
    state = {
        ingridients: [], error: ""
    }

    DeleteIngridient = async(id) =>{
        const {token} = this.props;
        try{
            const result = await fetch(url+id+'/',{
                method: "DELETE", headers:{
                    'Authorization': 'Token ' + token
                }
            });
            this.componentDidMount();
        }
        catch(error){
            this.setState({error:"Ошибка получения данных"})
        }
    }
    componentDidMount = async()=>{
        const {token} = this.props;
        let ingridients = [];
        try{
            const result = await fetch(url, {
                method:"GET", headers:{
                    'Authorization' : 'Token ' + token
                }
            })
            ingridients = await result.json();
        }
        catch (error){
            this.setState({error: "Ошибка получения данных"})
        }
        this.setState({ingridients})
    }

    columns = [
        {
            field:'id',
            headerName: 'id',
            width: 200,
        },
        {
            field:'name',
            headerName: 'Название',
            width: 200,
        },
        {
            field:'supplier',
            headerName: 'Поставщик',
            width: 200,
        },
        {
            field:'supply_date',
            headerName: 'Дата поставки',
            width: 200,
        },
        {
            field:'supply_before',
            headerName: 'Использовать до',
            width: 200,
        },
        {
            field:'actions',
            type:'actions',
            getActions: (params)=>[
                <GridActionsCellItem icon={<DeleteIcon/>} onClick={this.DeleteIngridient.bind(this, params.id)} label={"Delete"}/>
            ]
        }
    ]

    render() {
        const {error, ingridients} = this.state;
        console.log(ingridients);
        return(
            <div className={"ingridients"}>
                Ингридиенты
                <h2>{error}</h2>
                <DataGrid autoHeight rows={ingridients} columns={this.columns} pageSize={5} rowsPerPageOptions={[5]} components={{ Toolbar: QuickSearchToolbar}} />
            </div>
        )
    }
}