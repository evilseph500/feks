import React from "react";
import {DataGrid, GridToolbarQuickFilter, GridActionsCellItem} from '@mui/x-data-grid'
import Box from '@mui/material/Box';
import DeleteIcon from '@mui/material/Icon';

const url = "http://darcinc.pythonanywhere.com/api/cook/";
function QuickSearchToolbar() {
    return(
        <Box sx={{p:0.5, pb:0}}>
            <GridToolbarQuickFilter
                quickFilterParser={(searchInput)=>searchInput.split(',').map((value)=>value.trim()).filter((value)=>value !=='')}
            />
        </Box>
    );
}
export default class Cooks extends React.Component{
    state = {
        cooks: [], error: ""
    }

    DeleteCook = async(id) =>{
        const {token} = this.props;
        try{
            const result = await fetch(url+id+'/',{
                method: "DELETE", headers:{
                    'Authorization': 'Token ' + token
                }
            });
            this.componentDidMount();
        }
        catch(error){
            this.setState({error:"Ошибка получения данных"})
        }
    }
    componentDidMount = async()=>{
        const {token} = this.props;
        let cooks = [];
        try{
            const result = await fetch(url, {
                method:"GET", headers:{
                    'Authorization' : 'Token ' + token
                }
            })
            cooks = await result.json();
        }
        catch (error){
            this.setState({error: "Ошибка получения данных"})
        }
        this.setState({cooks})
    }

    columns = [
        {
            field:'id',
            headerName: 'id',
            width: 200,
        },
        {
            field:'FIO',
            headerName: 'ФИО',
            width: 200,
        },
        {
            field:'job_name',
            headerName: 'Должность',
            width: 200,
        },
        {
            field:'salary',
            headerName: 'Зарплата',
            width: 200,
        },
        {
            field:'actions',
            type:'actions',
            getActions: (params)=>[
                <GridActionsCellItem icon={<DeleteIcon/>} onClick={this.DeleteCook.bind(this, params.id)} label={"Delete"}/>
            ]
        }
    ]

    render() {
        const {error, cooks} = this.state;
        console.log(cooks);
        return(
            <div className={"cooks"}>
                Повара
                <h2>{error}</h2>
                <DataGrid autoHeight rows={cooks} columns={this.columns} pageSize={5} rowsPerPageOptions={[5]} components={{ Toolbar: QuickSearchToolbar }} />
            </div>
        )
    }
}
