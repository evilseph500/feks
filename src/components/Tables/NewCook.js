import React from "react";
import { Modal } from "@mui/material";
const COOK_URL = "http://darcinc.pythonanywhere.com/api/cook/"

export default class NewCook extends React.Component{
    state = {
        create_success: undefined
    }
    newCook = async() =>{
        const {token}=this.props;
        let FIO=this.FIORef.value;
        let job_name = this.job_nameRef.value;
        let salary = this.salaryRef.value;
        try {
            const result = await fetch(COOK_URL, {
              method: "POST",
              headers: {
             'Content-Type': 'application/json', 
             'Authorization': 'Token ' + token ,},
              body: JSON.stringify({
                'FIO': FIO,
                'job_name': job_name,
                'salary': salary,
              })
            })
            let tmp = await result.json();
            if ('id' in tmp) {
              this.setState({create_success: true})
            }
            else {
              console.log(tmp);
              this.setState({create_success: false})
            }
          } catch (err) {
            this.setState({
              error: "Ошибка получения данных"
            })
          }
    }
    render() {
        let {create_success} = this.state;
        return (
          <div className={'leftmodal'}>
            <div><h1>Новый повар</h1></div>
            <div>ФИО: <input type={"text"} name={"title"} size={"50"} maxLength={"100"} ref={ref => this.FIORef = ref} /> </div>
            <div>Должность: <input type={"text"} name={"title"} size={20} maxLength={"20"} ref={ref => this.job_nameRef = ref} /></div>
            <div>Зарплата: <input type={"number"} name={"year"} min={100} max={999999999} ref={ref => this.salaryRef = ref} /></div>
            <button onClick={this.newCook}>Добавить повара</button>
            {create_success? <div><h2 className={'success'}>Повар добавлен</h2></div>:
              <div></div>
              }
          </div>
        )
      }
}