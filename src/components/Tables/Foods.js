import React from "react";
import {DataGrid, GridToolbarQuickFilter, GridActionsCellItem} from '@mui/x-data-grid'
import Box from '@mui/material/Box';
import DeleteIcon from '@mui/material/Icon';

const url = "http://darcinc.pythonanywhere.com/api/food/";
function QuickSearchToolbar() {
    return(
        <Box sx={{p:0.5, pb:0}}>
            <GridToolbarQuickFilter
                quickFilterParser={(searchInput)=>searchInput.split(',').map((value)=>value.trim()).filter((value)=>value !=='')}
            />
        </Box>
    );
}
export default class Foods extends React.Component{
    state = {
        foods: [], error: "", cooks: [],
    }

    DeleteFood = async(id) =>{
        const {token} = this.props;
        try{
            const result = await fetch(url+id+'/',{
                method: "DELETE", headers:{
                    'Authorization': 'Token ' + token
                }
            });
            this.componentDidMount();
        }
        catch(error){
            this.setState({error:"Ошибка получения данных"})
        }
    }
    componentDidMount = async()=>{
        const {token} = this.props;
        let foods = [];
        try{
            const result = await fetch(url, {
                method:"GET", headers:{
                    'Authorization' : 'Token ' + token
                }
            })
            foods = await result.json();
        }
        catch (error){
            this.setState({error: "Ошибка получения данных"})
        }
        this.setState({foods})

        let cooks = [];
        try{
            const result = await fetch(url, {
                method:"GET", headers:{
                    'Authorization' : 'Token ' + token
                }
            })
            cooks = await result.json();
        }
        catch (error){
            this.setState({error: "Ошибка получения данных"})
        }
        this.setState({cooks})
    }
    
    columns = [
        {
            field:'name',
            headerName: 'Название',
            width: 200,
        },
        {
            field:'price',
            headerName: 'Цена',
            width: 200,
        },
        {
            field:'cooking_date',
            headerName: 'Дата приготовления',
            width: 200,
        },
        {
            field:'cook',
            headerName: 'Повар',
            width: 200,
        },
        {
            field:'actions',
            type:'actions',
            getActions: (params)=>[
                <GridActionsCellItem icon={<DeleteIcon/>} onClick={this.DeleteFood.bind(this, params.id)} label={"Delete"}/>
            ]
        }
    ]

    render() {
        const {error, foods} = this.state;
        console.log(foods);
        return(
            <div className={"foods"}>
                Еда
                <h2>{error}</h2>
                <DataGrid autoHeight rows={foods} columns={this.columns} pageSize={5} rowsPerPageOptions={[5]} components={{ Toolbar: QuickSearchToolbar }} />
            </div>
        )
    }
}
